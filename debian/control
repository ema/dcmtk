Source: dcmtk
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Gert Wollny <gewo@debian.org>,
           Mathieu Malaterre <malat@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), cmake,
               debhelper-compat (= 13),
               gettext,
               help2man,
               libpng-dev,
               libsndfile1-dev,
               libssl-dev,
               libtiff-dev,
               libwrap0-dev,
               libnsl-dev,
               libxml2-dev,
               zlib1g-dev
Build-Depends-Indep: doxygen,
                     graphviz [!armhf !armel]
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/dcmtk
Vcs-Git: https://salsa.debian.org/med-team/dcmtk.git
Homepage: https://dicom.offis.de/dcmtk
Rules-Requires-Root: no

Package: dcmtk
Architecture: any
Depends: adduser,
         ${misc:Depends},
         ${shlibs:Depends}
Description: OFFIS DICOM toolkit command line utilities
 DCMTK includes a collection of libraries and applications for examining,
 constructing and converting DICOM image files, handling offline media,
 sending and receiving images over a network connection, as well as
 demonstrative image storage and worklist servers.
 .
 This package contains the DCMTK utility applications.
 .
 Note: This version was compiled with libssl support.

Package: libdcmtk17t64
Provides: ${t64:Provides}
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Breaks: libdcmtk17 (<< ${source:Version}), libdcmtk16 (= 3.6.7-1)
Replaces: libdcmtk17, libdcmtk16 (= 3.6.7-1)
Multi-Arch: same
Description: OFFIS DICOM toolkit runtime libraries
 DCMTK includes a collection of libraries and applications for examining,
 constructing and converting DICOM image files, handling offline media,
 sending and receiving images over a network connection, as well as
 demonstrative image storage and worklist servers.
 .
 This package contains the runtime libraries for the DCMTK utility
 applications.
 .
 Note: This version was compiled with libssl support.

Package: libdcmtk-dev
Architecture: any
Section: libdevel
Depends: libdcmtk17t64 (= ${binary:Version}),
         libpng-dev,
         libssl-dev,
         libtiff-dev,
         libwrap0-dev,
         libnsl-dev,
         libxml2-dev,
         ${misc:Depends}
Suggests: dcmtk-doc
Provides: libdcmtk17-dev
Description: OFFIS DICOM toolkit development libraries and headers
 DCMTK includes a collection of libraries and applications for examining,
 constructing and converting DICOM image files, handling offline media,
 sending and receiving images over a network connection, as well as
 demonstrative image storage and worklist servers.
 .
 This package contains development libraries and headers for DCMTK. You
 only need to install this if you are developing programs that use the
 DCMTK libraries.
 .
 Note: This version was compiled with libssl support.

Package: dcmtk-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, libjs-jquery
Suggests: dcmtk,
          libdcmtk-dev
Multi-Arch: foreign
Description: OFFIS DICOM toolkit documentation
 DCMTK includes a collection of libraries and applications for examining,
 constructing and converting DICOM image files, handling offline media,
 sending and receiving images over a network connection, as well as
 demonstrative image storage and worklist servers.
 .
 This package contains the on-line documentation for the DCMTK libraries
 and utilities in HTML format.
